<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>REST Server Tests</title>

    <style>

    ::selection { background-color: #E13300; color: white; }
    ::-moz-selection { background-color: #E13300; color: white; }

    body {
        background-color: #FFF;
        margin: 40px;
        font: 16px/20px normal Helvetica, Arial, sans-serif;
        color: #4F5155;
        word-wrap: break-word;
    }

    a {
        color: #039;
        background-color: transparent;
        font-weight: normal;
    }

    h1 {
        color: #444;
        background-color: transparent;
        border-bottom: 1px solid #D0D0D0;
        font-size: 24px;
        font-weight: normal;
        margin: 0 0 14px 0;
        padding: 14px 15px 10px 15px;
    }

    code {
        font-family: Consolas, Monaco, Courier New, Courier, monospace;
        font-size: 16px;
        background-color: #f9f9f9;
        border: 1px solid #D0D0D0;
        color: #002166;
        display: block;
        margin: 14px 0 14px 0;
        padding: 12px 10px 12px 10px;
    }

    #body {
        margin: 0 15px 0 15px;
    }

    p.footer {
        text-align: right;
        font-size: 16px;
        border-top: 1px solid #D0D0D0;
        line-height: 32px;
        padding: 0 10px 0 10px;
        margin: 20px 0 0 0;
    }

    #container {
        margin: 10px;
        border: 1px solid #D0D0D0;
        box-shadow: 0 0 8px #D0D0D0;
    }
    </style>
</head>
<body>

<div id="container">
    <h1>Integracion Bancard</h1>

    <div id="body">

        <h2><a href="<?php echo site_url(); ?>">Sermed Use Cases</a></h2>

        <p>
            Haz Click en los links para chequear los siguientes casos de uso
        </p>

        <ol>
            <li><a href="<?php echo site_url('invoices/?tid=3949&prd_id=1&sub_id=4055086&addl=1'); ?>">Invoices (Exito)</a> - defaulting to JSON</li>
            <li><a href="<?php echo site_url('invoices/?tid=3949&prd_id=1&sub_id=4055087&addl=1'); ?>">Invoices (Usuario no encontrado)</a> - defaulting to JSON</li>
            <li><a href="<?php echo site_url('invoices/?tid=3949&prd_id=1&sub_id=8888&addl=1'); ?>">Invoices (Usuario no tiene facturas)</a> - defaulting to JSON</li>

            <li>
              <form class="" action="<?php echo site_url('payment/'); ?>" method="post">
                <input type="text" name="tid" value="1234">
                <input type="hidden" name="prd_id" value="1">
                <input type="text" placeholder="sub_id" name="sub_id[0]" value="44444405">
                <input type="text" name="inv_id[0]" placeholder="inv_id" value="60_20">
                <input type="hidden" name="amt" value="100000">
                <input type="hidden" name="curr" value="PYG">
                <input type="hidden" name="trn_dat" value="2016-12-12">
                <input type="hidden" name="trn_hou" value="223344">
                <input type="hidden" name="cm_amt" value="100000">
                <input type="hidden" name="cm_curr" value="PYG">
                <input type="hidden" name="addl" value="">
                <button type="submit" name="button">Payment</button> Payment (Usuario no tiene facturas)
              </form>
            </li>

            <li>
              <form class="" action="<?php echo site_url('payment/'); ?>" method="post">
                <input type="hidden" name="tid" value="1234">
                <input type="hidden" name="prd_id" value="1">
                <input type="hidden" name="sub_id" value="8888">
                <input type="hidden" name="inv_id" value="1234">
                <input type="hidden" name="amt" value="100000">
                <input type="hidden" name="curr" value="PYG">
                <input type="hidden" name="trn_dat" value="Miercoles 2016-12-12">
                <input type="hidden" name="trn_hou" value="223344">
                <input type="hidden" name="cm_amt" value="100000">
                <input type="hidden" name="cm_curr" value="PYG">
                <input type="hidden" name="addl" value="">
                <button type="submit" name="button">Payment</button> Payment (Parametros invalidos)
              </form>
            </li>

            <li>
              <form class="" action="<?php echo site_url('payment/'); ?>" method="post">
                <!-- <input type="hidden" name="tid" value="1234"> -->
                <input type="hidden" name="prd_id" value="1">
                <input type="hidden" name="sub_id" value="8888">
                <input type="hidden" name="inv_id" value="1234">
                <input type="hidden" name="amt" value="100000">
                <input type="hidden" name="curr" value="PYG">
                <input type="hidden" name="trn_dat" value="2016-12-12">
                <input type="hidden" name="trn_hou" value="223344">
                <input type="hidden" name="cm_amt" value="100000">
                <input type="hidden" name="cm_curr" value="PYG">
                <input type="hidden" name="addl" value="">
                <button type="submit" name="button">Payment</button> Payment (Parametros insuficientes)
              </form>
            </li>

            <li>
              <form class="" action="<?php echo site_url('payment/'); ?>" method="post">
                <input type="hidden" name="tid" value="1234">
                <input type="hidden" name="prd_id" value="1">
                <input type="hidden" name="sub_id" value="7777">
                <input type="hidden" name="inv_id" value="6666">
                <input type="hidden" name="amt" value="100000">
                <input type="hidden" name="curr" value="PYG">
                <input type="hidden" name="trn_dat" value="2016-12-12">
                <input type="hidden" name="trn_hou" value="223344">
                <input type="hidden" name="cm_amt" value="100000">
                <input type="hidden" name="cm_curr" value="PYG">
                <input type="hidden" name="addl" value="">
                <button type="submit" name="button">Payment</button> Payment (Factura vencida)
              </form>
            </li>

            <li>
              <form class="" action="<?php echo site_url('payment/'); ?>" method="post">
                <input type="hidden" name="tid" value="1234">
                <input type="hidden" name="prd_id" value="1">
                <input type="hidden" name="sub_id" value="7777">
                <input type="hidden" name="inv_id" value="1100">
                <input type="hidden" name="amt" value="100000">
                <input type="hidden" name="curr" value="PYG">
                <input type="hidden" name="trn_dat" value="2016-12-12">
                <input type="hidden" name="trn_hou" value="223344">
                <input type="hidden" name="cm_amt" value="100000">
                <input type="hidden" name="cm_curr" value="PYG">
                <input type="hidden" name="addl" value="">
                <button type="submit" name="button">Payment</button> Payment (No hay respuesta del autorizador)
              </form>
            </li>

            <li>
              <form class="" action="<?php echo site_url('payment/'); ?>" method="post">
                <input type="hidden" name="tid" value="1234">
                <input type="hidden" name="prd_id" value="1">
                <input type="hidden" name="sub_id" value="7777">
                <input type="hidden" name="inv_id" value="1101">
                <input type="hidden" name="amt" value="100000">
                <input type="hidden" name="curr" value="PYG">
                <input type="hidden" name="trn_dat" value="2016-12-12">
                <input type="hidden" name="trn_hou" value="223344">
                <input type="hidden" name="cm_amt" value="100000">
                <input type="hidden" name="cm_curr" value="PYG">
                <input type="hidden" name="addl" value="">
                <button type="submit" name="button">Payment</button> Payment (Error desconocido)
              </form>
            </li>

            <li>
              <form class="" action="<?php echo site_url('payment/'); ?>" method="post">
                <input type="hidden" name="tid" value="1234">
                <input type="hidden" name="prd_id" value="1">
                <input type="hidden" name="sub_id" value="7777">
                <input type="hidden" name="inv_id" value="1102">
                <input type="hidden" name="amt" value="100000">
                <input type="hidden" name="curr" value="PYG">
                <input type="hidden" name="trn_dat" value="2016-12-12">
                <input type="hidden" name="trn_hou" value="223344">
                <input type="hidden" name="cm_amt" value="100000">
                <input type="hidden" name="cm_curr" value="PYG">
                <input type="hidden" name="addl" value="">
                <button type="submit" name="button">Payment</button> Payment (Error en el autorizador)
              </form>
            </li>

            <li>
              <form class="" action="<?php echo site_url('payment/'); ?>" method="post">
                <input type="hidden" name="tid" value="1234">
                <input type="hidden" name="prd_id" value="1">
                <input type="hidden" name="sub_id" value="8889">
                <input type="hidden" name="inv_id" value="8889">
                <input type="hidden" name="amt" value="100000">
                <input type="hidden" name="curr" value="PYG">
                <input type="hidden" name="trn_dat" value="2016-12-12">
                <input type="hidden" name="trn_hou" value="223344">
                <input type="hidden" name="cm_amt" value="100000">
                <input type="hidden" name="cm_curr" value="PYG">
                <input type="hidden" name="addl" value="">
                <button type="submit" name="button">Payment</button> Payment (Pago no autorizado)
              </form>
            </li>

            <li>
              <form class="" action="<?php echo site_url('payment/'); ?>" method="post">
                <input type="text" placeholder="tid" name="tid" value="">
                <input type="hidden" name="prd_id" value="1">
                <input type="text" name="sub_id" placeholder="CI" value="">
                <input type="text" name="inv_id" placeholder="invoice id" value="">
                <input type="text" name="amt" placeholder="monto" value="">
                <input type="hidden" name="curr" value="PYG">
                <input type="hidden" name="trn_dat" value="2016-12-12">
                <input type="hidden" name="trn_hou" value="223344">
                <input type="hidden" name="cm_amt" value="0">
                <input type="hidden" name="cm_curr" value="PYG">
                <input type="hidden" name="addl" value="">
                <button type="submit" name="button">Payment</button> Payment (Exito)
              </form>
            </li>

            <li>
              <form class="" action="<?php echo site_url('reverse/'); ?>" method="post">
                <input type="text" name="tid" placeholder="tid" value="">
                <button type="submit" name="button">Reverse</button> Reverse (Ya fue reversada)
              </form>
            </li>

            <li>
              <form class="" action="<?php echo site_url('reverse/'); ?>" method="post">
                <!-- <input type="hidden" name="tid" value="8888"> -->
                <button type="submit" name="button">Reverse</button> Reverse (Parametros insuficientes)
              </form>
            </li>

            <li>
              <form class="" action="<?php echo site_url('reverse/'); ?>" method="post">
                <input type="text" name="tid" placeholder="tid" value="">
                <button type="submit" name="button">Reverse</button> Reverse (Transaccion irreversible)
              </form>
            </li>

            <li>
              <form class="" action="<?php echo site_url('reverse/'); ?>" method="post">
                <input type="text" name="tid" placeholder="tid" value="">
                <button type="submit" name="button">Reverse</button> Reverse (Reversa procesada con Exito)
              </form>
            </li>


        </ol>

    </div>

    <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>'.CI_VERSION.'</strong>' : '' ?></p>
</div>

<script src="https://code.jquery.com/jquery-1.12.0.js"></script>

<script>
    // Create an 'App' namespace
    var App = App || {};

    // Basic rest module using an IIFE as a way of enclosing private variables
    App.rest = (function restModule(window) {
        // Fields
        var _alert = window.alert;
        var _JSON = window.JSON;

        // Cache the jQuery selector
        var _$ajax = null;

        // Cache the jQuery object
        var $ = null;

        // Methods (private)

        /**
         * Called on Ajax done
         *
         * @return {undefined}
         */
        function _ajaxDone(data) {
            // The 'data' parameter is an array of objects that can be iterated over
            _alert(_JSON.stringify(data, null, 2));
        }

        /**
         * Called on Ajax fail
         *
         * @return {undefined}
         */
        function _ajaxFail() {
            _alert('Oh no! A problem with the Ajax request!');
        }

        /**
         * On Ajax request
         *
         * @param {jQuery} $element Current element selected
         * @return {undefined}
         */
        function _ajaxEvent($element) {
            $.ajax({
                    // URL from the link that was 'clicked' on
                    url: $element.attr('href')
                })
                .done(_ajaxDone)
                .fail(_ajaxFail);
        }

        /**
         * Bind events
         *
         * @return {undefined}
         */
        function _bindEvents() {
            // Namespace the 'click' event
            _$ajax.on('click.app.rest.module', function (event) {
                event.preventDefault();

                // Pass this to the Ajax event function
                _ajaxEvent($(this));
            });
        }

        /**
         * Cache the DOM node(s)
         *
         * @return {undefined}
         */
        function _cacheDom() {
            _$ajax = $('#ajax');
        }

        // Public API
        return {
            /**
             * Initialise the following module
             *
             * @param {object} jQuery Reference to jQuery
             * @return {undefined}
             */
            init: function init(jQuery) {
                $ = jQuery;

                // Cache the DOM and bind event(s)
                _cacheDom();
                _bindEvents();
            }
        };
    }(window));

    // DOM ready event
    $(function domReady($) {
        // Initialise the App module
        App.rest.init($);
    });
</script>

</body>
</html>
