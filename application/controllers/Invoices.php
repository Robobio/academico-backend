<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Invoices extends REST_Controller {

    function __construct(){
        // Construct the parent class
        parent::__construct();
        // $this->load->library('database');
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    //TODO get_next_dues
    private function get_next_dues($inv_id){
      $next_dues = new stdClass();
      $next_dues->date = '2012-07-21';
      $next_dues->amt = 100500;
      return $next_dues;
    }

    //TODO get_invoices
    private function get_invoices($sub_id, $prd_id){

      if(!is_array($sub_id)){
        $tmp = [];
        $tmp[] = $sub_id;
        $sub_id = $tmp;
      }else{
        $tmp = $sub_id;
      }

      $invoices = [];
      foreach ($tmp as $key => $value) {
        $sql = "select c.id_cliente,c.id_secuencia,c.ci cedula,h.fecha_vencimiento,h.saldo, h.saldo pago_minimo,h.nro_cuota,null,null,'PYG',null,null,'Cuota '||h.mes||'/'||h.anho dsc
        from chequera h,cliente c
        where c.id_cliente=h.clie_id_cliente and c.id_secuencia=h.sec_id_cliente
        and h.saldo > 0 and c.ci ='".$value."'";

        $query = $this->db->query($sql);
        foreach ($query->result() as $chequera_item) {
          $invoice = new stdClass();
          $cadena = "Cuota 2/2016";
          $partes1 = explode(" ", $cadena);
          $partes2 = explode("/", $partes1[1]);
          $mes = $partes2[0];
          $ano = $partes2[1];
          $ano = $ano + 2;
          $date = $mes . '-01-' . $ano;
          $invoice->due = date('Y-m-d', strtotime($date));
          $invoice->amt = $chequera_item->SALDO;
          $invoice->min_amt = $chequera_item->SALDO;
          $invoice->inv_id = Array($chequera_item->ID_CLIENTE . "_" . $chequera_item->NRO_CUOTA);
          $invoice->curr = 'PYG';
          $invoices[] = $invoice;
        }
      }
      return $invoices;
    }

    //TODO validate_sub_id
    private function validate_sub_id($sub_id){

      $valid = true;

      if(!is_array($sub_id)){
        $tmp = [];
        $tmp[] = $sub_id;
        $sub_id = $tmp;
      }else{
        $tmp = $sub_id;
      }

      foreach ($tmp as $key => $value) {
        $query = $this->db->query("select c.id_cliente,c.id_secuencia,c.ci cedula
        from cliente c
        where c.ci ='" . $value . "'");
        $result = $query->result();
        if(count($result) == 0){
          $valid = false;
        }
      }


      return $valid;



    }

    //TODO user_has_invoices
    private function user_has_invoices($sub_id){
      if(!is_array($sub_id)){
        $tmp = [];
        $tmp[] = $sub_id;
        $sub_id = $tmp;
      }else{
        $tmp = $sub_id;
      }

      $invoices = [];

      foreach ($tmp as $key => $value) {
        $invoices = array_merge($invoices, $this->get_invoices($value, 1) );
      }

      // var_dump($invoices);die();

      return (count($invoices)>0);
    }

    private function constructResponse($estado, $descripcionEstado, $tid, $invoices){
      $messagesResponses = array();
      $messagesResponses['QueryNotAllowed'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['InvalidParameters'] = REST_CONTROLLER::HTTP_UNPROCESSABLE_ENTITY;
      $messagesResponses['MissingParameters'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['SubscriberNotFound'] = REST_CONTROLLER::HTTP_NOT_FOUND;
      $messagesResponses['QueryNotProcessed'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['NoResponseFromHost'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['HostTransactionError'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['UnknownError'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['SubscriberWithoutDebt'] = REST_CONTROLLER::HTTP_OK;
      $messagesResponses['QueryProcessed'] = REST_CONTROLLER::HTTP_OK;

      $messagesLevels = array();
      $messagesLevels['QueryNotAllowed'] = 'error';
      $messagesLevels['InvalidParameters'] = 'error';
      $messagesLevels['MissingParameters'] = 'error';
      $messagesLevels['SubscriberNotFound'] = 'error';
      $messagesLevels['QueryNotProcessed'] = 'error';
      $messagesLevels['NoResponseFromHost'] = 'error';
      $messagesLevels['HostTransactionError'] = 'error';
      $messagesLevels['UnknownError'] = 'error';
      $messagesLevels['SubscriberWithoutDebt'] = 'info';
      $messagesLevels['QueryProcessed'] = 'success';

      $response = array();
      $response['status'] = $messagesLevels[$estado];
      $response['tid'] = (int) $tid;
      $response['messages'] = array();
      $response['invoices'] = array();
      $messages = array();
      $messages[0] = new stdClass();
      $messages[0]->level = $messagesLevels[$estado];
      $messages[0]->key = $estado;
      $messages[0]->dsc = Array($descripcionEstado);
      $response['messages'] = $messages;
      if($invoices !== null){
        $response['invoices'] = $invoices;
      }
      $this->response($response, $messagesResponses[$estado]);
    }

    // invoices endpoint
    public function index_get(){
      //Identificador de transacción
      $tid = $this->get('tid');

      //Identificador universal del producto.
      $prd_id = $this->get('prd_id');

      //Identificador del abonado. Tipo: StringList
      $sub_id = $this->get('sub_id');

      //Datos adicionales de la consulta.
      $addl = $this->get('addl');



      if($tid=='' || $prd_id=='' || $sub_id==''){
        $this->constructResponse('MissingParameters', "Parametros insuficientes", $tid, null);
      }else if(!$this->validate_sub_id($sub_id)){
        $this->constructResponse('SubscriberNotFound', "El codigo de abonado no fue encontrado", null, null);
      }else if(!$this->user_has_invoices($sub_id)){
        var_dump($sub_id);
        $this->constructResponse('SubscriberWithoutDebt', "El abonado no tiene facturas", null, []);
      }else{
        $invoices = $this->get_invoices($sub_id, $prd_id);
        $this->constructResponse('QueryProcessed', "Consulta procesada con exito", $tid, $invoices);
      }
    }

}
