<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Reverse extends REST_Controller {

    function __construct(){
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    private function constructResponse($estado, $descripcionEstado, $tid, $invoices, $aut_cod=false){
      $messagesResponses = array();
      $messagesResponses['AlreadyReversed'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['InvalidParameters'] = REST_CONTROLLER::HTTP_UNPROCESSABLE_ENTITY;
      $messagesResponses['MissingParameters'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['NoResponseFromHost'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['UnknownError'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['HostTransactionError'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['TransactionNotReversed'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['TransactionReversed'] = REST_CONTROLLER::HTTP_OK;

      $messagesLevels = array();
      $messagesLevels['AlreadyReversed'] = 'error';
      $messagesLevels['InvalidParameters'] = 'error';
      $messagesLevels['MissingParameters'] = 'error';
      $messagesLevels['NoResponseFromHost'] = 'error';
      $messagesLevels['HostTransactionError'] = 'error';
      $messagesLevels['UnknownError'] = 'error';
      $messagesLevels['TransactionNotReversed'] = 'error';
      $messagesLevels['TransactionReversed'] = 'success';


      $response = array();
      $response['status'] = $messagesLevels[$estado];
      $response['tid'] = (int) $tid;
      $response['messages'] = array();
      $messages = new stdClass();
      $messages->level = $messagesLevels[$estado];
      $messages->key = $estado;
      $messages->dsc = Array($descripcionEstado);
      $response['messages'] = $messages;
      if($invoices !== null){
        $messages->invoices = $invoices;
      }
      if($aut_cod){
          $response["aut_cod"] = $aut_cod;
      }
      $this->response($response, $messagesResponses[$estado]);
    }

    private function alreadyReversed($tid){
      //Atrapar con error de timeout de la DB, esto es una respuesta simulada

      // if($tid == 1100){
      //   $this->constructResponse('NoResponseFromHost', "No hubo respuesta del Host/Autorizador", $tid, null);
      //   die();
      // }
      // //Atrapar con cualquier error que no sea timeout o transaccional
      // if($tid == 1101){
      //   $this->constructResponse('UnknownError', "Error desconocido", $tid, null);
      //   die();
      // }
      //
      // //Atrapar con cualquier error transaccional de la DB
      // if($tid == 1102){
      //   $this->constructResponse('HostTransactionError', "Error en el Host/Autorizador", $tid, null);
      //   die();
      // }

      $sql = "select * from trans_bancard t where t.reversed = 'SI' and t.chequera_cod_transaccion = ". $tid;
      $query = $this->db->query($sql);
      $result = $query->result();
      return count($result) > 0;
    }

    private function reverseTransacion($tid){
      $this->db->trans_start();
      $sql = "select * from trans_bancard t where t.chequera_cod_transaccion = ". $tid;
      $query = $this->db->query($sql);
      $transaccion = $query->result()[0];

      $tid = $transaccion->CHEQUERA_COD_TRANSACCION;
      $amt = $transaccion->MONTO_TRANSACCION;
      $cuota = $transaccion->NRO_CUOTA;
      $ci = $transaccion->CI;

      $sql1 = "update chequera t set t.red_pago_id_red_pago=1,t.saldo=t.saldo+".$amt." ,t.fecha_pago=sysdate,t.cod_transaccion=".$tid." where t.clie_id_cliente = (select c.id_cliente from cliente c where c.ci='".$ci."') and t.sec_id_cliente=(select e.id_secuencia from cliente e where e.ci='".$ci."') and t.nro_cuota=".$cuota;
      $query = $this->db->query($sql1);

      $sql2 = "update trans_bancard t set t.reversed = 'SI' where t.chequera_cod_transaccion=".$tid;
      $query = $this->db->query($sql2);
      $this->db->trans_complete();

      //la transaccion 8889 no se puede reversar
      return true;
    }

    public function index_post(){
      //Identificador de transacción
      $tid = $this->post('tid');

      if($tid==''){
        $this->constructResponse('MissingParameters', "Parametros insuficientes", $tid, null);
        return false;
      }

      if($this->alreadyReversed($tid)){
        $this->constructResponse('AlreadyReversed', "La transaccion ya fue reversada", $tid, null);
        return false;
      }

      if($this->reverseTransacion($tid)){
        $this->constructResponse('TransactionReversed', "La transaccion fue reversada con exito", $tid, null);
        return false;
      }else{
        $this->constructResponse('TransactionNotReversed', "La transaccion es irreversible", $tid, null);
        return false;
      }

    }

}
