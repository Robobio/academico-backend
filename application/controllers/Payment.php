<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Payment extends REST_Controller {

    function __construct(){
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    private function constructResponse($estado, $descripcionEstado, $tid, $invoices, $aut_cod=false){
      $messagesResponses = array();
      $messagesResponses['QueryNotAllowed'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['InvalidParameters'] = REST_CONTROLLER::HTTP_UNPROCESSABLE_ENTITY;
      $messagesResponses['MissingParameters'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['OverdueInvoice'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['SubscriberNotFound'] = REST_CONTROLLER::HTTP_NOT_FOUND;
      $messagesResponses['QueryNotProcessed'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['NoResponseFromHost'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['HostTransactionError'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['UnknownError'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['SubscriberWithoutDebt'] = REST_CONTROLLER::HTTP_FORBIDDEN;
      $messagesResponses['PaymentProcessed'] = REST_CONTROLLER::HTTP_OK;
      $messagesResponses['PaymentAuthorized'] = REST_CONTROLLER::HTTP_OK;
      $messagesResponses['PaymentNotAuthorized'] = REST_CONTROLLER::HTTP_FORBIDDEN;

      $messagesLevels = array();
      $messagesLevels['QueryNotAllowed'] = 'error';
      $messagesLevels['InvalidParameters'] = 'error';
      $messagesLevels['MissingParameters'] = 'error';
      $messagesLevels['OverdueInvoice'] = 'error';
      $messagesLevels['SubscriberNotFound'] = 'error';
      $messagesLevels['QueryNotProcessed'] = 'error';
      $messagesLevels['NoResponseFromHost'] = 'error';
      $messagesLevels['HostTransactionError'] = 'error';
      $messagesLevels['UnknownError'] = 'error';
      $messagesLevels['SubscriberWithoutDebt'] = 'error';
      $messagesLevels['PaymentProcessed'] = 'success';
      $messagesLevels['PaymentAuthorized'] = 'success';
      $messagesLevels['PaymentNotAuthorized'] = 'error';

      $response = array();
      $response['status'] = $messagesLevels[$estado];
      $response['tid'] = (int) $tid;
      $response['messages'] = array();
      $messages = new stdClass();
      $messages->level = $messagesLevels[$estado];
      $messages->key = $estado;
      $messages->dsc = Array($descripcionEstado);
      $response['messages'] = $messages;
      if($invoices !== null){
        $messages->invoices = $invoices;
      }
      if($aut_cod){
          $response["aut_cod"] = $aut_cod;
      }
      $this->response($response, $messagesResponses[$estado]);
    }

    //TODO user_has_invoices
    private function user_has_invoices($sub_id){
      //El 8888 no tiene facturas
      return ($sub_id != 8888);
    }

    private function valid_date($date){
      return ((preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) || (preg_match("/^[0-9]{4}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)));
    }

    //TODO invoice_overdue
    private function invoice_overdue($inv_id){
      return ($inv_id != 6666);
    }

    //TODO authorizePayment
    private function authorizePayment($tid, $inv_id, $prd_id, $sub_id, $amt, $curr, $trn_dat, $trn_hou, $cm_amt, $cm_curr){
      //Atrapar con error de timeout de la DB, esto es una respuesta simulada
      if($inv_id == 1100){
        $this->constructResponse('NoResponseFromHost', "No hubo respuesta del Host/Autorizador", null, null);
        die();
      }
      //Atrapar con cualquier error que no sea timeout o transaccional
      if($inv_id == 1101){
        $this->constructResponse('UnknownError', "Error desconocido", null, null);
        die();
      }

      //Atrapar con cualquier error transaccional de la DB
      if($inv_id == 1102){
        $this->constructResponse('HostTransactionError', "Error en el Host/Autorizador", null, null);
        die();
      }


      // var_dump($inv_id);
      $partes = explode("_", $inv_id[0]);
      // var_dump($partes);

      $sql1 = "update chequera t set t.red_pago_id_red_pago=1,t.saldo=t.saldo-".$amt." ,t.fecha_pago=sysdate,t.cod_transaccion=".$tid." where t.clie_id_cliente = (select c.id_cliente from cliente c where c.ci='".$sub_id[0]."') and t.sec_id_cliente=(select e.id_secuencia from cliente e where e.ci='".$sub_id[0]."') and t.nro_cuota=".$partes[1];

      // var_dump($sql1);
      // die();

      $this->db->trans_start();
      $sql1 = "update chequera t set t.red_pago_id_red_pago=1,t.saldo=t.saldo-".$amt." ,t.fecha_pago=sysdate,t.cod_transaccion=".$tid." where t.clie_id_cliente = (select c.id_cliente from cliente c where c.ci='".$sub_id[0]."') and t.sec_id_cliente=(select e.id_secuencia from cliente e where e.ci='".$sub_id[0]."') and t.nro_cuota=".$partes[1];
      // echo $sql1; die();
      $query = $this->db->query($sql1);
      $sql2 = "insert into trans_bancard values (".$tid.", ".$amt.", ".$partes[1].", '".$sub_id[0]."', 'NO')";
      $query = $this->db->query($sql2);
      $this->db->trans_complete();

      // $this->db->_error_message(); (mysql_error equivalent)
      // $this->db->_error_number(); (mysql_errno equivalent)

      return true;
    }

    public function index_post(){
      //Identificador de transacción
      $tid = $this->post('tid');

      //Identificador universal del producto.
      $prd_id = $this->post('prd_id');

      //Identificador del abonado. Tipo: StringList
      $sub_id = $this->post('sub_id');

      //Identificador de Factura o Cuota
      $inv_id = $this->post('inv_id');

      //Importe de la transaccion
      $amt = $this->post('amt');

      //Moneda de la factura
      $curr = $this->post('curr');

      //Fecha de la transaccion
      $trn_dat = $this->post('trn_dat');

      //Hora de la transaccion
      $trn_hou = $this->post('trn_hou');

      //Importe de la comision
      $cm_amt = $this->post('cm_amt');

      //Moneda de la comision
      $cm_curr = $this->post('cm_curr');

      //Datos adicionales del pago
      $addl = $this->post('addl');

      if(
        $tid=='' ||
        $prd_id=='' ||
        $sub_id=='' ||
        $inv_id=='' ||
        $amt=='' ||
        $curr=='' ||
        $trn_dat=='' ||
        $trn_hou=='' ||
        $cm_amt=='' ||
        $cm_curr==''){
        $this->constructResponse('MissingParameters', "Parametros insuficientes", $tid, null);
        return false;
      }

      if(!$this->valid_date($trn_dat)){
        $this->constructResponse('InvalidParameters', "Error en los parametros", null, null);
        return false;
      }

      if(!$this->user_has_invoices($sub_id)){
        $this->constructResponse('SubscriberWithoutDebt', "El abonado no tiene deuda pendiente", $tid, null);
        return false;
      }

      if(!$this->invoice_overdue($inv_id)){
        $this->constructResponse('OverdueInvoice', "El pago no fue aprobado, factura ya esta vencida", $tid, null);
        return false;
      }

      if(!$this->authorizePayment($tid, $inv_id, $prd_id, $sub_id, $amt, $curr, $trn_dat, $trn_hou, $cm_amt, $cm_curr)){
        $this->constructResponse('PaymentNotAuthorized', "El pago no fue autorizado", $tid, null);
      }else{
        $this->constructResponse('PaymentProcessed', "Pago procesado con exito", $tid, null);
        return true;

      }
    }

}
